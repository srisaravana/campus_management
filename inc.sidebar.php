<?php

use App\Core\App;

?>

<div class="sidebar-section">
    <div class="sidebar-logo">
        <i class="fas fa-university"></i> CAMS
    </div>
</div>

<div class="sidebar-section">
    <div class="sidebar-section-header">Main</div>

    <div class="sidebar-section-body">
        <div class="links-list">
            <a href="<?= App::siteURL() ?>/students/register.php" class=""><i class="fas fa-home"></i> Dashboard</a>
        </div>
    </div>
</div>

<div class="sidebar-section">
    <div class="sidebar-section-header">Students</div>

    <div class="sidebar-section-body">
        <div class="links-list">
            <a href="<?= App::siteURL() ?>/students/register.php" class=""><i class="fas fa-user-plus"></i> Add a new student</a>
            <a href="<?= App::siteURL() ?>/students/register.php" class=""><i class="fas fa-user-plus"></i> Manage all</a>
        </div>
    </div>

</div>



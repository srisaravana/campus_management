<?php

use App\Core\App;
use App\Core\Auth;
use App\Core\Requests\JSONResponse;

require_once "../inc.start.php";


$fields = [
    'email' => $_POST['email'] ?? null,
    'password' => $_POST['password'] ?? null
];


try {

    if ( empty($fields['email']) || empty($fields['password']) ) {
        throw new Exception("Username or password missing");
    }

    if ( Auth::authenticate($fields['email'], $fields['password']) ) {
        JSONResponse::validResponse("Authenticated");
        return;
    } else {
        throw new Exception("Login failed!");
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}
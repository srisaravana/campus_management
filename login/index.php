<?php

use App\Core\App;

require_once "../inc.start.php";

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>CAMS: LOGIN</title>
    <link rel="stylesheet" href="<?= App::siteURL() ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= App::siteURL() ?>/assets/css/app.css">
</head>

<body>
<div class="container mt-5">

    <div class="row justify-content-center">
        <div class="col-6">

            <div class="alert alert-primary">

                <h4 class="text-center">Login to <?= App::appName() ?></h4>
                <hr>

                <form id="form_login">

                    <div class="form-group">
                        <label for="field_email">Email</label>
                        <input type="text" class="form-control" id="field_email" name="email" value="admin@example.com">
                        <div class="invalid-feedback">Email is required.</div>
                    </div>

                    <div class="form-group">
                        <label for="field_password">Password</label>
                        <input type="password" class="form-control" id="field_password" name="password" value="admin">
                    </div>

                    <div class="text-right">
                        <button class="btn btn-success" type="submit" id="btn_login">Login</button>
                    </div>

                </form>

            </div>

        </div>
    </div>

</div>


<script src="<?= App::siteURL() ?>/assets/js/jquery-3.5.1.min.js"></script>
<script src="<?= App::siteURL() ?>/assets/js/popper.min.js"></script>
<script src="<?= App::siteURL() ?>/assets/js/bootstrap.bundle.js"></script>
<script src="<?= App::siteURL() ?>/assets/js/app.js"></script>
</body>
</html>


<script src="login.js"></script>
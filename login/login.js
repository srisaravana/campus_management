$(function () {



    $("#form_login").on("submit", function (e) {

        e.preventDefault();

        console.log("login button clicked");

        let fields = {
            fieldEmail: $("#field_email"),
            fieldPassword: $("#field_password")
        };

        let validated = true;

        if (isEmptyField(fields.fieldEmail)) {
            validated = false;
        }

        if (isEmptyField(fields.fieldPassword)) {
            validated = false;
        }


        if (validated) {

            $.post(`${getSiteURL()}/login/process_login.php`, {
                email: getStringValue(fields.fieldEmail),
                password: getStringValue(fields.fieldPassword)
            }).done(function (response) {

                redirect(getSiteURL());

            }).fail(function (error) {
                console.log(error);
                alert(error.responseJSON.message);
            });

        }


    });


});
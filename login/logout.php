<?php

use App\Core\App;

require_once "../inc.start.php";


session_unset();
session_destroy();

App::redirect('/');
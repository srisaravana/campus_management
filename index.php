<?php

use App\Core\App;
use App\Core\Auth;

require_once "inc.start.php";

App::setTitle("Home");

// check if user is logged in before rendering the page
Auth::checkAuthentication();

?>


<?php include_once "inc.header.php"; ?>



        <div class="container-fluid mt-3">
            <div class="row">
                <div class="col">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto atque consectetur exercitationem facere
                    harum, iure laboriosam modi molestiae neque nisi, perferendis quibusdam quo
                    quod reiciendis voluptate voluptatem voluptatibus voluptatum? Amet!
                </div>

                <div class="col">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto atque consectetur exercitationem facere
                    harum, iure laboriosam modi molestiae neque nisi, perferendis quibusdam quo
                    quod reiciendis voluptate voluptatem voluptatibus voluptatum? Amet!


                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action active">
                            Cras justo odio
                        </a>
                        <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
                        <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
                        <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
                        <a href="#" class="list-group-item list-group-item-action disabled" tabindex="-1" aria-disabled="true">Vestibulum at eros</a>
                    </div>

                </div>
            </div>
        </div>


<?php include_once "inc.footer.php"; ?>
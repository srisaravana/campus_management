<?php

declare(strict_types=1);

include_once "../inc.start.php";

use App\Core\Requests\JSONResponse;
use App\Models\Student;
use App\Models\User;

$fields = [
    'full_name' => $_POST['full_name'] ?? null,
    'dob' => $_POST['dob'] ?? null,
    'gender' => $_POST['gender'] ?? null,
    'nic' => $_POST['nic'] ?? null,
    'contact_number' => $_POST['contact_number'] ?? null,
    'address' => $_POST['address'] ?? null,
    'district_id' => $_POST['district_id'] ?? null,
    'postal_code' => $_POST['postal_code'] ?? null,
];


try {

    if ( empty($fields['full_name']) || empty($fields['dob']) || empty($fields['gender']) || empty($fields['nic'])) {
        throw new Exception("Missing required fields.");
    }

    $student = Student::create($fields);

    $result = $student->save();

    if ( !empty($result) ) {
        $student = Student::select($result);
        JSONResponse::validResponse(['student' => $student]);
    }


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}
<?php

use App\Core\App;
use App\Core\Auth;
use App\FormHelper;
use App\Models\District;
use App\Models\Student;

require_once "../inc.start.php";

// check if user is logged in before rendering the page
Auth::checkAuthentication();

$districts = District::selectAll();


?>

<?php include_once BASE_PATH . "/inc.header.php"; ?>


<div class="container">

    <div class="row">
        <div class="col-12">

            <h2>Register new student</h2>
            <hr>

            <div class="row">

                <div class="col-12">


                    <form>


                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label for="text_full_name">Full Name</label>
                                    <input type="text" id="text_full_name" name="full_name" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="text_dob">Date of Birth</label>
                                    <input type="date" id="text_dob" name="dob" class="form-control">
                                </div>
                            </div>


                            <div class="col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="text_gender">Gender</label>
                                    <select name="gender" id="text_gender" class="form-control">
                                        <option value="0" disabled selected>SELECT</option>
                                        <?php FormHelper::createSelectOptions(Student::GENDERS); ?>
                                    </select>
                                </div>
                            </div>

                        </div><!--end row-->


                        <div class="row">
                            <div class="col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="text_nic">NIC</label>
                                    <input type="text" id="text_nic" name="nic" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="text_contact_number">Contact Number</label>
                                    <input type="text" id="text_contact_number" name="contact_number" class="form-control">
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12 col-lg-6">
                                <div class="form-group">
                                    <label for="text_address">Address</label>
                                    <textarea name="address" id="text_address" rows="4" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="text_district_id">District</label>
                                    <select name="district_id" id="text_district_id" class="form-control">
                                        <option value="0" disabled selected>SELECT</option>

                                        <?php foreach ( $districts as $district ): ?>
                                            <option value="<?= $district->id ?>"><?= $district->district ?></option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12 col-lg-3">
                                <div class="form-group">
                                    <label for="text_postal_code">Postal Code</label>
                                    <input type="text" id="text_postal_code" name="postal_code" class="form-control" placeholder="Eg. 30000">
                                </div>
                            </div>

                        </div><!--end row-->

                        <div class="row">
                            <div class="col">
                                <button type="button" class="btn btn-success" id="btn_register_student">Save</button>
                                <a href="#" class="btn btn-warning">Cancel</a>
                            </div>
                        </div>


                    </form>


                </div>
            </div>

        </div>
    </div>

</div>


<?php include_once BASE_PATH . "/inc.footer.php"; ?>
<script src="register.js"></script>

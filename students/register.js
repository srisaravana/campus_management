$(function () {

    // set date of birth as today
    $("#text_dob").val(moment().format("YYYY-MM-DD"));


    $("#btn_register_student").on("click", function () {

        let fields = {
            fieldFullName: $("#text_full_name"),
            fieldDob: $("#text_dob"),
            fieldGender: $("#text_gender"),
            fieldNic: $("#text_nic"),
            fieldContactNumber: $("#text_contact_number"),
            fieldAddress: $("#text_address"),
            fieldDistrictId: $("#text_district_id"),
            fieldPostalCode: $("#text_postal_code"),
        };


        console.log(fields.fieldGender.val());
        console.log(fields.fieldPostalCode.val());

        let validated = true;

        if (isEmptyField(fields.fieldFullName)) validated = false;
        if (isEmptyField(fields.fieldGender)) validated = false;
        if (isEmptyField(fields.fieldDistrictId)) validated = false;
        if (isEmptyField(fields.fieldNic)) validated = false;


        if (validated) {

            $.post(`${getSiteURL()}/students/process_add_student.php`, {
                // 'full_name': getStringValue(fields.fieldFullName),
                'dob': getStringValue(fields.fieldDob),
                'gender': getStringValue(fields.fieldGender),
                'nic': getStringValue(fields.fieldNic),
                'contact_number': getStringValue(fields.fieldContactNumber),
                'address': getStringValue(fields.fieldAddress),
                'district_id': getIntegerValue(fields.fieldDistrictId),
                'postal_code': getIntegerValue(fields.fieldPostalCode),
            }).done(function (response) {

            }).fail(function (error) {
                console.log(error.responseJSON.message);
            });

        } else {
            alert("Please fill in the required fields marked in red.");
        }

    });

});
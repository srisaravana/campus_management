<?php

use App\Core\App;
use App\Core\Auth;

require_once "inc.start.php";


?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?= App::getTitle() ?></title>
    <link rel="stylesheet" href="<?= App::siteURL() ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= App::siteURL() ?>/assets/plugins/fontawesome-pro-5.13.0-web/css/all.min.css">
    <link rel="stylesheet" href="<?= App::siteURL() ?>/assets/css/app.css">
</head>
<body>


<div class="f-container">


    <div class="content">

        <div class="f-item sidebar pt-3 px-2">

            <?php include_once BASE_PATH . "/inc.sidebar.php"; ?>

        </div>

        <div class="f-item main-content">


            <div id="top-nav" class="mb-3">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="<?= App::siteURL() ?>">Home</a>
                            </li>

                        </ul>
                        <form class="form-inline mr-auto">
                            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </form>

                        <div>
                            <a href="<?= App::siteURL() ?>/users/manage_users.php" class="btn btn-outline-dark my-2 my-sm-0">Manage Users</a>
                            <a href="<?= App::siteURL() ?>/login/logout.php" class="btn btn-outline-danger my-2 my-sm-0">Logout</a>
                        </div>

                    </div>
                </nav>
            </div>

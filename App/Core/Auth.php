<?php


namespace App\Core;


use App\Models\User;

class Auth
{
    public static function authenticate(string $email, string $password)
    {
        $existingUser = User::selectByEmail($email);

        if ( !empty($existingUser) ) {
            if ( password_verify($password, $existingUser->salt) ) {
                self::initUserSession($existingUser);
                return true;

            }
        }

        return false;

    }


    public static function initUserSession(User $user)
    {
        $_SESSION['user'] = [
            'email' => $user->email,
            'display_name' => $user->display_name,
            'role' => $user->role
        ];
    }


    private static function _checkAuthentication($role = User::ROLE_USER)
    {
        if ( isset($_SESSION['user']) ) {
            if ( $_SESSION['user']['role'] == $role ) {
                return true;
            }
        }
        return false;
    }

    public static function checkAuthentication()
    {
        if ( !(self::_checkAuthentication(User::ROLE_ADMIN) || self::_checkAuthentication(User::ROLE_USER)) ) {
            App::redirect("/login");
        }
    }

    public static function checkAdminAuthentication()
    {
        if ( !self::_checkAuthentication(User::ROLE_ADMIN) ) {
            App::redirect("/login");
        }
    }

    public static function isAuthenticated(){
        if ( !(self::_checkAuthentication(User::ROLE_ADMIN) || self::_checkAuthentication(User::ROLE_USER)) ) {
            return false;
        }
        return true;
    }

}


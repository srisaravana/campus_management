<?php

namespace App\Models;

interface IModel
{
    public static function select(int $id);

    public static function selectAll(int $limit = 1000, int $offset = 0);

    public function save();

    public function update();

    public function delete();
}
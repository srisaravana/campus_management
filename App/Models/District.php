<?php


namespace App\Models;


use App\Core\Database\Database;

class District implements IModel
{

    private const TABLE_NAME = "districts";

    public ?int $id;
    public ?string $district;

    /**
     * @param int $id
     * @return District|null
     */
    public static function select(int $id)
    {
        return Database::find(self::TABLE_NAME, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return District[]|null
     */
    public static function selectAll(int $limit = 1000, int $offset = 0)
    {
        return Database::findAll(self::TABLE_NAME, $limit, $offset, self::class);
    }

    public function save()
    {
        // TODO: Implement save() method.
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}
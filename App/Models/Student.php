<?php


namespace App\Models;


use App\Core\Database\Database;

class Student implements IModel
{

    private const TABLE_NAME = "students";

    public ?int $id;
    public ?string $full_name, $nic, $gender, $dob, $contact_number, $address, $district_id, $postal_code;

    public const GENDERS = [
        'MALE' => 'Male',
        'FEMALE' => 'Female',
        'OTHER' => 'Other'
    ];


    /**
     * @param $fields
     * @return Student
     */
    public static function create($fields)
    {
        $object = new self();
        foreach ( $fields as $key => $value ) {
            $object->$key = $value;
        }

        return $object;
    }

    /**
     * @param int $id
     * @return Student|null
     */
    public static function select(int $id)
    {
        return Database::find(self::TABLE_NAME, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return Student[]|null
     */
    public static function selectAll(int $limit = 1000, int $offset = 0)
    {
        return Database::findAll(self::TABLE_NAME, $limit, $offset, self::class);
    }

    public function save()
    {
        $data = [
            'full_name' => $this->full_name,
            'nic' => $this->nic,
            'gender' => $this->gender,
            'dob' => $this->dob,
            'contact_number' => $this->contact_number,
            'address' => $this->address,
            'district_id' => $this->district_id,
            'postal_code' => $this->postal_code,
        ];

        return Database::insert(self::TABLE_NAME, $data);
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }

    public function getDistrict()
    {
        return District::select($this->district_id);
    }
}
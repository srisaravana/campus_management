<?php
declare(strict_types=1);

namespace App\Models;

use App\Core\Database\Database;

class User implements IModel
{

    private const TABLE = "users";

    public ?int $id;
    public ?string $display_name, $email, $salt, $role;

    public const ROLES = [
        'ADMIN' => 'Administrator',
        'USER' => 'User'
    ];

    public const ROLE_ADMIN = 'ADMIN';
    public const ROLE_USER = 'USER';

    /**
     * @param int $id
     * @return User|null
     */
    public static function select(int $id)
    {
        return Database::find(self::TABLE, $id, self::class);
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return User[]
     */
    public static function selectAll(int $limit = 1000, int $offset = 0)
    {
        return Database::findAll(self::TABLE, $limit, $offset, self::class);
    }

    public function save()
    {
        $data = [
            'display_name' => $this->display_name,
            'email' => $this->email,
            'role' => $this->role,
            'salt' => $this->salt,
        ];

        return Database::insert(self::TABLE, $data);
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }

    /**
     * @param $email
     * @return User
     */
    public static function selectByEmail($email)
    {

        $db = Database::instance();

        $statement = $db->prepare("select * from users where email = ?");

        $statement->execute([$email]);

        return $statement->fetchObject(self::class);
    }


}
<?php

declare(strict_types=1);

session_start();

require_once "vendor/autoload.php";

use App\Core\App;
use App\Core\Auth;
use App\Core\Database\Database;


$dbConfig = [
    'HOST' => 'localhost',
    'DATABASE' => 'bit_campus_learning',
    'USERNAME' => 'root',
    'PASSWORD' => '',
];

Database::init($dbConfig);




define('BASE_PATH', __DIR__);
define('APP_NAME', 'Campus Management');
define('APP_VERSION', 0.05);


App::setTitle("");



<?php

declare(strict_types=1);

include_once "../inc.start.php";

use App\Core\Requests\JSONResponse;
use App\Models\User;

$fields = [
    'display_name' => $_POST['display_name'] ?? null,
    'email' => $_POST['email'] ?? null,
    'role' => $_POST['role'] ?? null,
    'password' => $_POST['password'] ?? null,
];

try {

    if ( empty($fields['email']) || empty($fields['display_name']) || empty($fields['password']) ) {
        throw new Exception("Required fields missing");
    }

    // check if email already exist for another user
    $temp = User::selectByEmail($fields['email']);

    if(!empty($temp)){
        throw new Exception('User already exist for the email given');
    }

    $user = new User();
    $user->display_name = $fields['display_name'];
    $user->email = $fields['email'];
    $user->role = $fields['role'];
    $user->salt = password_hash($fields['password'], PASSWORD_DEFAULT);

    $result = $user->save();

    if ( !empty($result) ) {

        $addedUser = User::select($result);

        JSONResponse::validResponse(['user' => $addedUser]);
        return;
    }

    throw new Exception('Failed to add a new user');


} catch ( Exception $exception ) {
    JSONResponse::exceptionResponse($exception);
}
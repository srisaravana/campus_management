<?php

declare(strict_types=1);

use App\Core\App;
use App\Core\Auth;
use App\FormHelper;
use App\Models\User;

require_once "../inc.start.php";

App::setTitle("Add users");

// check if user is logged in before rendering the page
Auth::checkAdminAuthentication();

?>


<?php include_once BASE_PATH . "/inc.header.php"; ?>

<div class="container">
    <div class="row justify-content-center">

       <div class="col col-md-6">
           <div class="card">
               <div class="card-header">Add a new user</div>
               <div class="card-body">

                   <form>

                       <div class="form-group">
                           <label for="f_display_name">Display name</label>
                           <input type="text" class="form-control" id="f_display_name">
                       </div>

                       <div class="form-group">
                           <label for="f_email">Email</label>
                           <input type="text" class="form-control" id="f_email">
                       </div>

                       <div class="form-group">
                           <label for="f_role">User role</label>
                           <select id="f_role" class="form-control">
                               <?php FormHelper::createSelectOptions(User::ROLES, User::ROLE_USER); ?>
                           </select>
                       </div>

                       <div class="form-group">
                           <label for="f_password">Password</label>
                           <input type="password" class="form-control" id="f_password">
                       </div>

                       <div class="text-right">
                           <button id="btn_save_user" class="btn btn-success" type="button">Save</button>
                           <a class="btn btn-warning" href="#">Cancel</a>
                       </div>

                   </form>

               </div>
           </div>
       </div>

    </div>
</div>


<?php include_once BASE_PATH . "/inc.footer.php"; ?>
<script src="add.js"></script>

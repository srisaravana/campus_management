<?php

declare(strict_types=1);

use App\Core\App;
use App\Core\Auth;
use App\FormHelper;
use App\Models\User;

require_once "../inc.start.php";

App::setTitle("Manage Users");

// check if user is logged in before rendering the page
Auth::checkAdminAuthentication();


$users = User::selectAll();

?>


<?php include_once BASE_PATH . "/inc.header.php"; ?>

<div class="container">
    <div class="row justify-content-center">

        <div class="col">
            <div class="card">
                <div class="card-header">Manage Users</div>
                <div class="card-body">

                    <div class="mb-3">
                        <a href="<?= App::url('/users/add.php') ?>" class="btn btn-primary">Add new user</a>
                    </div>


                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Role</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ( $users as $user ): ?>
                            <tr>
                                <td><a href="<?= App::url('/users/edit.php', ['id' => $user->id]) ?>"><?= $user->display_name ?></a></td>
                                <td><?= $user->email ?></td>
                                <td><?= $user->role ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
</div>


<?php include_once BASE_PATH . "/inc.footer.php"; ?>
<script src="add.js"></script>

$(function () {

    $("#btn_save_user").on("click", function () {

        let fields = {
            fieldDisplayName: $("#f_display_name"),
            fieldEmail: $("#f_email"),
            fieldRole: $("#f_role"),
            fieldPassword: $("#f_password"),
        };

        let validated = true;

        if (isEmptyField(fields.fieldDisplayName) || isEmptyField(fields.fieldEmail) || isEmptyField(fields.fieldPassword)) {
            validated = false;
        }

        if (validated) {

            $.post(`${getSiteURL()}/users/process_add.php`, {
                display_name: getStringValue(fields.fieldDisplayName),
                email: getStringValue(fields.fieldEmail),
                role: getStringValue(fields.fieldRole),
                password: getStringValue(fields.fieldPassword),

            }).done(function (response) {

                console.log(response);

            }).fail(function (error) {
                alert(error.responseJSON.message);
            });

        }


    });

});